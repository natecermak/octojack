#include <MultiStepper.h>
#include <AccelStepper.h>
#include <Servo.h> 


#define SERVO0 6
#define SERVO1 5
#define SERVO2 4
#define SERVO3 3

#define STEPPER0_DIR  7 
#define STEPPER0_STEP 8
#define STEPPER0_NEN  9
#define STEPPER1_DIR  10
#define STEPPER1_STEP 11
#define STEPPER1_NEN  12
#define STEPPER2_DIR  24
#define STEPPER2_STEP 25
#define STEPPER2_NEN  26

#define GPIO0 29
#define GPIO1 30
#define GPIO2 31
#define GPIO3 32
#define GPIO4 35
#define GPIO5 36
#define DAC0 A21
#define DAC1 A22

#define DCMOTOR_PWMA 14
#define DCMOTOR_AI1  16
#define DCMOTOR_AI2  15
#define DCMOTOR_BI1  18
#define DCMOTOR_BI2  19
#define DCMOTOR_PWMB 20
#define DCMOTOR_STBY 17

#define RELAY0 39
#define RELAY1 38
#define RELAY2 37

#define LED0 21
#define LED1 22

Servo servo0, servo1, servo2, servo3;

AccelStepper stepper0(1, STEPPER0_STEP, STEPPER0_DIR); 
AccelStepper stepper1(1, STEPPER1_STEP, STEPPER1_DIR); 
AccelStepper stepper2(1, STEPPER2_STEP, STEPPER2_DIR); 


void setup() {
  // put your setup code here, to run once:
  pinMode(SERVO0, OUTPUT);
  pinMode(SERVO1, OUTPUT);
  pinMode(SERVO2, OUTPUT);
  pinMode(SERVO3, OUTPUT);

  pinMode(STEPPER0_DIR, OUTPUT);
  pinMode(STEPPER0_STEP, OUTPUT);
  pinMode(STEPPER0_NEN, OUTPUT);
  pinMode(STEPPER1_DIR, OUTPUT);
  pinMode(STEPPER1_STEP, OUTPUT);
  pinMode(STEPPER1_NEN, OUTPUT);
  pinMode(STEPPER2_DIR, OUTPUT);
  pinMode(STEPPER2_STEP, OUTPUT);
  pinMode(STEPPER2_NEN, OUTPUT);  
  digitalWrite(STEPPER0_NEN, HIGH);  
  digitalWrite(STEPPER1_NEN, HIGH);  
  digitalWrite(STEPPER2_NEN, HIGH);  

  pinMode(DCMOTOR_PWMA, OUTPUT);
  pinMode(DCMOTOR_AI1, OUTPUT);
  pinMode(DCMOTOR_AI2, OUTPUT);
  pinMode(DCMOTOR_BI1, OUTPUT);
  pinMode(DCMOTOR_BI2, OUTPUT); 
  pinMode(DCMOTOR_PWMB, OUTPUT);
  pinMode(DCMOTOR_STBY, OUTPUT);

  pinMode(RELAY0, OUTPUT);
  pinMode(RELAY1, OUTPUT);
  pinMode(RELAY2, OUTPUT);

  pinMode(LED0, OUTPUT);
  pinMode(LED1, OUTPUT);
 
  servo0.attach(SERVO0);
  servo1.attach(SERVO1);
  servo2.attach(SERVO2);
  servo3.attach(SERVO3);

  stepper0.setMaxSpeed(500);
  stepper1.setMaxSpeed(500);
  stepper2.setMaxSpeed(500);
  stepper0.setAcceleration(1000);
  stepper1.setAcceleration(1000);
  stepper2.setAcceleration(1000);

  pinMode(GPIO0, ??);
  pinMode(GPIO1, ??);
  pinMode(GPIO2, ??);
  pinMode(GPIO3, ??);
  pinMode(GPIO4, ??);
  pinMode(GPIO5, ??);
  pinMode(DAC0, ??);
  pinMode(DAC1, ??);
  
  Serial.begin(115200);
}

void loop() {

  /*
  // SERVO TEST
  servo0.write(90+30*sin(0.007*millis()));              
  servo1.write(90+30*sin(0.008*millis()));
  servo2.write(90+30*sin(0.009*millis()));
  servo3.write(90+30*sin(0.010*millis()));
  */
  
  /*
  //STEPPER TEST 
  digitalWrite(STEPPER0_NEN, LOW);
  digitalWrite(STEPPER1_NEN, LOW);
  digitalWrite(STEPPER2_NEN, LOW);
  for(int i = 0; i < 1000; i++) {
    digitalWrite(STEPPER0_STEP, HIGH);
    digitalWrite(STEPPER0_STEP, LOW);
    digitalWrite(STEPPER1_STEP, HIGH);
    digitalWrite(STEPPER1_STEP, LOW);
    digitalWrite(STEPPER2_STEP, HIGH);
    digitalWrite(STEPPER2_STEP, LOW);
    delay(3);                       
  } 
  digitalWrite(STEPPER0_NEN, HIGH);
  digitalWrite(STEPPER1_NEN, HIGH);
  digitalWrite(STEPPER2_NEN, HIGH);
  */

  /*
  //ACCELSTEPPER TEST
  digitalWrite(STEPPER0_NEN, LOW);
  digitalWrite(STEPPER1_NEN, LOW);
  digitalWrite(STEPPER2_NEN, LOW);
  stepper0.runToNewPosition(1000);
  stepper0.runToNewPosition(000);
  stepper1.runToNewPosition(1000);
  stepper1.runToNewPosition(000);
  stepper2.runToNewPosition(1000);
  stepper2.runToNewPosition(000);
  */
  
}
