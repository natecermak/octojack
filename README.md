## octojack!
![octojack!](images/octojack_logo.png)
![octojack!](images/octojack_box.png)

Octojack is a breakout-box for the [Teensy 3.6](https://www.pjrc.com/teensy/techspecs.html), an exceptionally capable microcontroller. Octojack is designed to allow the Teensy to easily connect with a lot of common things in homebrewed (and especially laboratory-related) instruments. Octojack can connect to things via the following input/output (I/O):

- **6 General purpose input/output (GPIO) pins, accessible as BNC connections (6/6), male pin headers (6/6), and push terminals (2 out of 6).** Each GPIO has a range of functions including digital I/O (6/6), pulse width modulation (PWM; on 4 GPIOs), analog input (on 6 GPIOs), and touch sensing (on 2 GPIOs).
- **2 Analog output pins, accessible as BNC connections, male pin headers, and push terminals.** Octojack breaks out the Teensy's 2 built-in DACs (12 bit, 0-3.3V) for easy connections.
- **3 stepper motor outputs, connectable via male pin headers and push terminals.** These are driven by [DRV8825 modules](https://www.pololu.com/product/2133) (or A4988 modules). Their power supplies can be set to 5V or 12, and their microstepping mode can be set using jumpers.
- **4 servo motor outputs, connectable via male pin headers.** These 3-pin connections include ground, +5V, and a PWM signal.
- **2 DC motor outputs, connectable via male pin headers and push terminals.** These are driven by a [TB6612fng](https://www.sparkfun.com/products/14451) breakout board, and speed and direction are controllable. Power can be from the 5V or 12V supply, selected via a jumper
- **3 solid state relays, connectable via male pin headers and push terminals.** These can handle up to 700mA of current and can be powered from either 5V or 12V, selected via jumper. 
- **2 330mA LED outputs, connectable via male pin headers and push terminals.** These are based on the [PicoBuck LED driver](https://www.sparkfun.com/products/13705), and can be used to drive 330mA LEDs. 


Octojack was originally motivated by the [BNC-2090A](http://sine.ni.com/nips/cds/view/p/lang/en/nid/203462), a breakout board for National Instruments (NI) data acquisition (DAQ) devices. The name octojack comes as a compromise between an octopus, with many arms for doing many things simultaneously, and a jack-of-all-trades, for doing many jobs well. 

Note that the Teensy 3.6 is not 5V-tolerant, but by default octojack protects GPIO inputs to make them 5V tolerant.

## How do I put together an octojack?

### Ordering
You can build an octojack! You will need to purchase [about $100 worth of components](octojack_BOM.xlsx) from [Digikey](https://www.digikey.com/) and a [printed circuit board (PCB)](octojack_fabFiles_mainBoard.zip) and [panels for the box](octojack_fabFiles_panel.zip) from any of a number of companies - I often use [JLCPCB](https://jlcpcb.com) or [seeedstudio](https://www.seeedstudio.com/fusion_pcb.html). 

To order the main board, upload the [main board gerber files](octojack_fabFiles_mainBoard.zip) to your PCB fabrication site of choice. Most of the default options should be fine (FR-4, 1.6mm thick, any color is fine, etc), but you'll need to select a 4-layer board, and if you need to enter the board dimensions, the main board is 159 mm by 79.5 mm in size.

To order the panels (the front and back of the box), upload the [panel gerber files](octojack_fabFiles_panel.zip) to your PCB fabrication site of choice. Select 2-layer board, size 170 mm by 108.5 mm. At least with [JLCPCB](https://jlcpcb.com) and [seeedstudio](https://www.seeedstudio.com/fusion_pcb.html), you also need to enter that there are two designs in this file. 

Typically I order 5 of each board, since the cost is essentially the same as ordering 1 board. 

### Assembly (Soldering!)
Once you've got the main board, panels and [components](octojack_BOM.xlsx), you you'll have to do some soldering. I suggest you have the following handy:
 
 - soldering iron
 - solder paste
 - solder wire
 - tweezers
 - safety glasses
 
I suggest soldering components in the following order, as some components occlude access to others.

1. Start with all the surface mount components. These are all on the top side of the board.
2. Pushbutton (SW1), goes **on back side of board**. Before soldering, cut off about 2mm from each leg so that the pins are nearly flush with the board.
3. Right angle 3x4 pin header (U3) **on back side of board** and C8 (front side).
4. Teensy female pin headers (2 24-pin female headers).
5. 2-pin male headers (J11-15, J17, J24 and J28, J33) **on the back side of the board!**. Break these off as you need them from from a 40-pin header.  
6. Push terminals (7 four-position and 1 six-position), but first cut off about 2mm from each leg so that pins barely protrude from the other side of the board. Note that two of these terminals go **on the back side of the board** (J29 and J31).
7. All right angle connectors EXCEPT J4 **on the back side of the board!**
8. 2x3-pin headers for J7, J18, J27, and 3-pin headers for J16,J21, and J30, **all on the back side of the board!**
9. 8-pin female pin header sockets for A1 A2 and A3
10. capacitors C1-3.
11. 3-pin headers J3, J5, J6 and J32 **on the back of the board!**
12. 8-pin female pin header sockets for U2
13. Power plugs (J1, J2)
14. Eight BNC connectors for GPIO.

Next, attach jumpers to select the desired power source (5V or 12V) for each output (relays, DC motors, and stepper motors). Also attach jumpers to select the desired microstepping mode for the stepper motors (using the DRV8825 datasheet).

Attach the Teensy, TB6612FNG breakout board, and DRV8825 modules by gently pressing them into their sockets. If needed, set the current limits for the DRV8825 stepper motor driver modules.

Finally, place the entire board inside the enclosure, attach the panels, and plug in the USB! You're done!





## Programming and testing the octojack

Now you've got to program it to do what you want! The easiest way is by installing the [Arduino IDE](https://www.arduino.cc/en/main/software) and [Teensyduino](https://www.pjrc.com/teensy/td_download.html). Use the [basic template sketch](octojack_teensy/octojack_teensy.ino) provided that includes definitions of all the pins and a few helpful objects (AccelStepper, Servo) to control them.

You may also want to reference the [schematic](octojack_schematic.pdf) to check which pins go to which inputs and outputs.

